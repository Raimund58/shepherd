<?php

namespace App\Command;

use App\Entity\Blend;
use App\Entity\Frame;
use App\Entity\Task;
use App\Entity\Tile;
use App\Repository\TaskRepository;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\FlockStore;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * A console command that execute waiting task.
 *
 *     $ php bin/console app:task
 *
 */
class TasksExecuteCommand extends Command {
    protected static $defaultName = 'shepherd:tasks';

    /**
     * @var ContainerBagInterface
     */
    private $containerBag;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var TaskService
     */
    private $taskService;

    /**
     * @var LockInterface
     */
    private $lockWorker = null;

    /**
     * @var LockFactory
     */
    private $lockFactory;

    public function __construct(ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, TaskRepository $taskRepository, TaskService $taskService, LoggerInterface $logger) {
        parent::__construct();

        $this->containerBag = $containerBag;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
        $this->taskService = $taskService;

        $store = new FlockStore(sys_get_temp_dir());
        $this->lockFactory = new LockFactory($store);

    }

    protected function configure() : void {
        $this->setDescription('Execute all async tasks (zip, mp4 generation, ....)');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {
        if ($this->canWork()) {
            do {
                $status = $this->workOneTime();
            }
            while ($status == true);
        }

        $this->shutdown();

        return 0;
    }

    /**
     * @return bool|mixed true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTask() {
        $lock = $this->lockFactory->createLock('shepherd.task.search');

        $ret = true;

        if ($lock->acquire(true)) {
            $ret = $this->nextTasksActual();
            $lock->release();
        }

        return $ret;
    }

    /**
     * @return bool|mixed true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTasksActual() {
        /**
         * @var Task[]
         */
        $this->entityManager->clear(Task::class);
        $this->entityManager->clear(Blend::class);
        $this->entityManager->clear(Frame::class);
        $this->entityManager->clear(Tile::class);

        $tasks = $this->taskRepository->findby(array(), array('id' => 'ASC')); // ordered findAll

        $processingBlends = array();

        foreach ($tasks as $task) {
            if ($task->getStatus() == Task::STATUS_RUNNING) {
                $blend = $this->getBlendFromTask($task);
                if (is_object($blend)) {
                    $processingBlends[$blend->getId()] = $blend->getId();
                }
            }
            elseif ($task->getStatus() == Task::STATUS_WAITING) {
                $blend = $this->getBlendFromTask($task);
                if (is_object($blend) && in_array($blend->getId(), $processingBlends)) {
                    continue;
                }

                $task->setStatus(Task::STATUS_RUNNING);
                $this->entityManager->persist($task);
                $this->entityManager->flush();
                return $task;
            }
        }

        return true;
    }

    private function getBlendFromTask(Task $task) : ?Blend {
        // previous task didn't have mandatory blend id
        // for now try to found it
        // TODO: on next release, remove that function
        $blend = $task->getBlend();
        if (is_object($blend)) {
            return $blend;
        }
        elseif (is_object($task->getFrame())) {
            return $task->getFrame()->getBlend();
        }
        elseif (is_object($task->getTile())) {
            return $task->getTile()->getFrame()->getBlend();
        }
        else {
            return null;
        }
    }

    private function workOneTime() {
        $task = $this->nextTask();
        if ($task === true) { // no job
            return false;
        }
        elseif ($task === false) { // error
            return false;
        }
        elseif (is_object($task)) {
            $this->executeTask($task);
            return true; // silent the error for $this->work()
        }
    }

    private function executeTask(Task $task) : bool {
        $ret = $this->taskService->execute($task);

        // for now even if the task failed, remove it.
        // TODO: maybe add it back ??

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return $ret;
    }

    private function canWork() {
        $concurrent_tasks = (int)($this->containerBag->get('concurrent_tasks'));
        for ($i = 1; $i <= $concurrent_tasks; $i++) {
            $lock = $this->lockFactory->createLock("shepherd.task.$i.pid");
            if ($lock->acquire(false)) {
                $this->lockWorker = $lock;
                return true;
            }
        }

        return false;
    }

    private function shutdown() {
        if (is_null($this->lockWorker) == false) {
            $this->lockWorker->release();
        }
    }

}
