<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Monitoring\MonitoringCpu;
use App\Monitoring\MonitoringDisk;
use App\Monitoring\MonitoringHttpd;
use App\Monitoring\MonitoringNetwork;
use App\Monitoring\MonitoringRam;
use App\Monitoring\MonitoringTask;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\HttpdConfigService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request from Master server
 * @Route("/master")
 */
class MasterController {

    /**
     * Add a blend to the shepherd
     * @Route("/blend", methods="POST")
     */
    public function addBlend(Request $request, ContainerBagInterface $containerBag, BlendService $blendService) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {

            return new Response('', 403);
        }

        $data = json_decode($request->getContent(), true);
        if (array_key_exists('blend', $data) == false ||
            array_key_exists('framerate', $data) == false ||
            array_key_exists('width', $data) == false ||
            array_key_exists('height', $data) == false ||
            array_key_exists('mp4', $data) == false ||
            array_key_exists('image_extension', $data) == false ||
            array_key_exists('frames', $data) == false) {
            return new Response('', 400);
        }

        if ($blendService->addBlend($data['blend'], $data['image_extension'], $data['framerate'], $data['width'], $data['height'], $data['mp4'], $data['frames'])) {
            return new Response('', 200);
        }
        else {
            return new Response('', 500);
        }
    }

    /**
     * Remove a blend to the shepherd
     * @Route("/blend/{blend}/remove", methods="GET")
     */
    public function delBlend(Request $request, Blend $blend, EntityManagerInterface $entityManager, ContainerBagInterface $containerBag) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        // do not directly delete the blend because it will take a lot of time
        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_DELETE_BLEND);
        $taskZip->setBlend($blend);

        $entityManager->persist($taskZip);
        $entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Get blends infos
     * @Route("/blend/list", methods="GET")
     */
    public function getBlendList(Request $request, ContainerBagInterface $containerBag, BlendRepository $blendRepository) : JsonResponse {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $data = array();
        foreach ($blendRepository->findAll() as $k => $blend) {
            $data [] = array('id' => $blend->getId(), 'size' => $blend->getSize());
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Get storage usage for a blend
     * @Route("/blend/{blend}/storage", methods="GET")
     */
    public function storageUsageBlend(Request $request, Blend $blend, ContainerBagInterface $containerBag) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        return new Response($blend->getSize(), 200);
    }

    /**
     * set a rendering tile
     * @Route("/tile/{uid}/rendering/{token}", methods="GET")
     */
    public function tileRendering(Request $request, string $uid, string $token, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, TileRepository $tileRepository) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        $tile = $tileRepository->find($uid);
        if (is_null($tile)) {
            return new Response('', 404);
        }

        $tile->setStatus(Tile::STATUS_PROCESSING);
        $tile->setToken($token);

        $entityManager->persist($tile);
        $entityManager->flush();

        return new Response('', 200);
    }

    /**
     * reset a tile
     * @Route("/tile/{uid}/reset", methods="GET")
     */
    public function tileReset(Request $request, string $uid, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, TileRepository $tileRepository, FrameRepository $frameRepository, BlendRepository $blendRepository, BlendService $blendService) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        $tile = $tileRepository->find($uid);
        if (is_null($tile)) {
            return new Response('', 404);
        }

        $tile->setStatus(Tile::STATUS_WAITING);

        $entityManager->persist($tile);
        $entityManager->flush();

        // remove tile, frame, mp4, zip file too
        $tilePath = $tileRepository->getStorageDirectory($tile).DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension();
        $framePath = $frameRepository->getFullPath($tile->getFrame());
        $frameThumbnailPath = $frameRepository->getThumbnailPath($tile->getFrame());
        $blendMP4PreviewPath = $blendRepository->getMP4PreviewPath($tile->getFrame()->getBlend());
        $blendMP4FinalPath = $blendRepository->getMP4FinalPath($tile->getFrame()->getBlend());
        $blendZipPath = $blendRepository->getZip($tile->getFrame()->getBlend());

        // update blend storage usage
        $blendService->updateSize($tile->getFrame()->getBlend(), -1.0 * (@filesize($tilePath) + @filesize($framePath) + @filesize($frameThumbnailPath) + @filesize($blendMP4PreviewPath) + @filesize($blendMP4FinalPath) + @filesize($blendZipPath)));

        @unlink($tilePath);
        @unlink($framePath);
        @unlink($frameThumbnailPath);
        @unlink($blendMP4PreviewPath);
        @unlink($blendMP4FinalPath);
        @unlink($blendZipPath);

        return new Response('', 200);
    }

    /**
     * Set a owner token for a blend,
     * it be used a security to access assets
     * @Route("/blend/{blend}/token/owner", methods="POST")
     */
    public function setOwnerToken(Request $request, Blend $blend, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        $token = $request->get('token');
        $validity = $request->get('validity');

        if (is_null($token) || is_null($validity)) {
            return new Response('', 400);
        }

        $blend->setOwnerToken($token);

        $date = new DateTime();
        $date->setTimestamp($validity);
        $blend->setOwnerTokenValidity($date);

        $entityManager->persist($blend);
        $entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Set a thubmnail token for a blend,
     * it be used a security to access thumbnails
     * @Route("/blend/{blend}/token/thumbnail", methods="POST")
     */
    public function setThumbnailToken(Request $request, Blend $blend, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        $token = $request->get('token');
        $validity = $request->get('validity');

        if (is_null($token) || is_null($validity)) {
            return new Response('', 400);
        }

        $blend->setThumbnailToken($token);

        $date = new DateTime();
        $date->setTimestamp($validity);
        $blend->setThumbnailTokenValidity($date);

        $entityManager->persist($blend);
        $entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Set generation of MP4s,
     * @Route("/blend/{blend}/generatemp4/{mp4}", methods="GET")
     * @return Response
     */
    public function setGenerateMp4(Request $request, Blend $blend, bool $mp4, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, BlendRepository $blendRepository) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }
        $oldValue = $blend->getGenerateMp4();

        $blend->setGenerateMp4($mp4);

        $entityManager->persist($blend);
        $entityManager->flush();

        if ($oldValue == false && $blend->getGenerateMp4()) {
            if ($blendRepository->isFinished($blend)) {
                $taskMP4Final = new Task();
                $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
                $taskMP4Final->setBlend($blend);

                $taskMP4Preview = new Task();
                $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
                $taskMP4Preview->setBlend($blend);

                $entityManager->persist($taskMP4Final);
                $entityManager->persist($taskMP4Preview);
                $entityManager->flush();
            }
        }

        return new Response('', 200);
    }

    /**
     * Ask to generate a partial archive
     * @Route("/blend/{blend}/generatepartialarchive", methods="GET")
     * @return Response
     */
    public function generatePartialArchive(Request $request, Blend $blend,ContainerBagInterface $containerBag, EntityManagerInterface $entityManager) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $entityManager->persist($taskZip);
        $entityManager->flush();
        return new Response('', 200);
    }

    /**
     * Set a token for a blend,
     * it be used a security to access data
     * @Route("/monitoring", methods="GET")
     */
    public function getMonitoring(Request $request, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager, BlendRepository $blendRepository, HttpdConfigService $httpdConfigService) : JsonResponse {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $data = array();
        $data['max-request'] = array('value' => $httpdConfigService->getSavedMaxRequest());
        foreach (array(new MonitoringHttpd(), new MonitoringCpu(), new MonitoringRam(), new MonitoringNetwork(), new MonitoringDisk($blendRepository), new MonitoringTask($entityManager)) as $monitor) {
            $data[$monitor->getType()] = array('value' => $monitor->getValue());
            if ($monitor->hasMax()) {
                $data[$monitor->getType()]['max'] = $monitor->getMax();
            }
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Get current version
     * @Route("/version", methods="GET")
     */
    public function getVersion(Request $request, ContainerBagInterface $containerBag) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        return new Response(getenv('VERSION'), 200);
    }

    /**
     * Get all tasks
     * @Route("/tasks", methods="GET")
     */
    public function getTasks(Request $request, ContainerBagInterface $containerBag, TaskRepository $taskRepository) : JsonResponse {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $data = array();
        foreach ($taskRepository->findAll() as $task) {
            $arr = array('id' => $task->getId(), 'status' => $task->getStatus(), 'type' => $task->getType());
            if (is_object($task->getTile())) {
                $arr['tile'] = $task->getTile()->getId();
            }
            if (is_object($task->getFrame())) {
                $arr['frame'] = $task->getFrame()->getId();
            }
            if (is_object($task->getBlend())) {
                $arr['blend'] = $task->getBlend()->getId();
            }
            $data [] = $arr;
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Reset a task
     * @Route("/task/{task}/reset", methods="GET")
     */
    public function resetTask(Task $task, Request $request, ContainerBagInterface $containerBag, EntityManagerInterface $entityManager) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $task->setStatus(Task::STATUS_WAITING);

        $entityManager->persist($task);
        $entityManager->flush();
        return new Response('', 200);
    }

    /**
     * I'm alive!
     * @Route("/alive", methods="GET")
     */
    public function iamalive(Request $request, ContainerBagInterface $containerBag) : Response {
        if ($this->isComingFromMaster($containerBag, $request) == false) {
            return new Response('', 403);
        }

        return new Response('yes', 200);
    }

    private function isComingFromMaster(ContainerBagInterface $containerBag, Request $request) : bool {
        // TODO: not enough security...

        $remote = $request->getClientIp();
        $ipv4 = $containerBag->get('master_ipv4');
        $ipv6 = $containerBag->get('master_ipv6');

        return is_string($remote) && (IpUtils::checkIp($remote, $ipv4) || IpUtils::checkIp($remote, $ipv6));
    }
}