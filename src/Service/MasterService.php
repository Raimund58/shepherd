<?php


namespace App\Service;


use App\Entity\Blend;
use App\Entity\Tile;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MasterService {

    /**
     * @var string
     */
    private $serverUrl;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ContainerBagInterface $containerBag, LoggerInterface $logger) {
        $this->serverUrl = $containerBag->get('master_url').'/api/shepherd/';
        $this->logger = $logger;
    }

    public function notifyGeneratedMP4Final(Blend $blend) : bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'finish.php', array(
                'body' => array(
                    'type' => 'mp4_final',
                    'blend' => $blend->getId()
                )
            ));
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function notifyGeneratedMP4Preview(Blend $blend) : bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'finish.php', array(
                'body' => array(
                    'type' => 'mp4_preview',
                    'blend' => $blend->getId()
                )
            ));
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function notifyGeneratedZIP(Blend $blend) : bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'finish.php', array(
                'body' => array(
                    'type' => 'zip',
                    'blend' => $blend->getId()
                )
            ));
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function notifyTileFinished(Tile $tile, int $renderTime, int $memoryUser) : bool {
        $this->logger->debug(__method__.' '.$tile);
        try {
            $this->logger->debug(__method__.' do request '.$this->serverUrl.'finish.php');
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'finish.php', array(
                'body' => array(
                    'type' => 'tile',
                    'rendertime' => $renderTime,
                    'memory_used' => $memoryUser,
                    'tile' => $tile->getId()
                )
            ));
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            $this->logger->debug(__method__.' failed to validate tile on master '.$e);
            return false;
        }
    }

    public function sendMonitoring(string $json) : bool {
        $this->logger->error(__method__.' json: '.$json);

        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'monitoring.php', ['body' => $json]);
            return $response->getStatusCode() == 200;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }
}