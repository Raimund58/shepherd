<?php


namespace App\Monitoring;


use App\Tool\Size;

class MonitoringNetwork extends MonitoringComponentAbstract {
    public function getType() : string {
        return 'network';
    }

    private function getRawValue() : array {
        $interface = null;
        foreach (glob("/sys/class/net/*") as $anInterface) {
            if (basename($anInterface) != 'lo') {
                $interface = basename($anInterface);
                break;
            }
        }

        if (is_null($interface)) {
            return array('tx' => 0, 'rx' => 0);
        }

        $rx[] = @file_get_contents("/sys/class/net/$interface/statistics/rx_bytes");
        $tx[] = @file_get_contents("/sys/class/net/$interface/statistics/tx_bytes");
        sleep(1);
        $rx[] = @file_get_contents("/sys/class/net/$interface/statistics/rx_bytes");
        $tx[] = @file_get_contents("/sys/class/net/$interface/statistics/tx_bytes");

        $tbps = (double)($tx[1]) - (double)($tx[0]);
        $rbps = (double)($rx[1]) - (double)($rx[0]);

        return array('tx' => $tbps, 'rx' => $rbps);
    }

    public function getValue() : float {
        return $this->getRawValue()['tx'];
    }

    public function getHumanValue() : string {
        $raw = $this->getRawValue();
        return 'rx: '.Size::humanSize($raw['rx'], 'bps').' tx: '.Size::humanSize($raw['tx'], 'bps');
    }

}
