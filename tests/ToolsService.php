<?php


namespace App\Tests;


use App\Service\BlendService;

class ToolsService {
    /**
     * @var BlendService
     */
    private $blendService;


    public function __construct(BlendService $blendService) {
        $this->blendService = $blendService;
    }

    public function addBlend(string $type, int $framesCount, int $tilesCount, int $width, int $height) {
        $i = 0;
        $id = rand(1000, 20000);

        $frames = array();

        for ($f = 0; $f < $framesCount; $f++) {
            $tiles = array();
            for ($t = 0; $t < $tilesCount; $t++) {
                $tiles [] = array('uid' => $i++, 'number' => $t, 'image_extension' => 'png');
            }
            $frames [] = array('type' => $type, 'uid' => uniqid(), 'number' => $f, 'image_extension' => 'png', 'tiles' => $tiles);

        }
        $this->blendService->addBlend($id, 'png', 25, $width, $height, true, $frames);

        return $id;
    }
}