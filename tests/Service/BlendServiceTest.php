<?php


namespace App\Tests\Service;

use App\Entity\Blend;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use App\Service\TileService;
use App\Tests\Tools;
use App\Tests\ToolsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BlendServiceTest extends KernelTestCase {
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var TileService
     */
    private $tileService;

    /**
     * @var FrameService
     */
    private $frameService;

    /**
     * @var BlendService
     */
    private $blendService;

    /**
     * @var BlendRepository
     */
    private $blendRepository;

    /**
     * @var FrameRepository
     */
    private $frameRepository;

    /**
     * @var TileRepository
     */
    private $tileRepository;

    /**
     * @var ToolsService
     */
    private $toolsService;

    /**
     * @var Tools
     */
    private $tools;

    /**
     * @var Blend
     */
    private $blend;

    protected function setUp() : void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->blendRepository = self::$container->get('App\Repository\BlendRepository');
        $this->frameRepository = self::$container->get('App\Repository\FrameRepository');
        $this->tileRepository = self::$container->get('App\Repository\TileRepository');
        $this->blendService = self::$container->get('App\Service\BlendService');
        $this->frameService = self::$container->get('App\Service\FrameService');
        $this->tileService = self::$container->get('App\Service\TileService');
        $this->toolsService = new ToolsService($this->blendService);
        $this->tools = new Tools($this->entityManager, $this->tileRepository, $this->frameRepository, $this->frameService);

        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        foreach ($this->blend->getFrames() as $frame) {
            $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

            $this->assertTrue($this->frameService->generateImage($frame));
        }
    }

    public function testMP4PreviewGeneration() {
        $this->assertTrue($this->blendService->generateMP4Preview($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'_preview.'.'mp4');
    }

    public function testMP4FinalGeneration() {
        $this->assertTrue($this->blendService->generateMP4Final($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'_final.'.'mp4');
    }

    public function testZIPGeneration() {
        $this->assertTrue($this->blendService->generateZIP($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'.'.'zip');
    }

}